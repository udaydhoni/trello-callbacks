const cards = require('./cards.json');

function cardsFromlists (listID,callback) {
    let result = []
    for (let keys in cards) {
        if (keys == listID){
            result = cards[keys];
        }
    }
    setTimeout(()=>{
        if (result.length == 0) {
            callback("No cards for this list")
        } else {
            callback(result)
        }
        
    },2000);
}

module.exports = cardsFromlists;