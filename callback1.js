let boards = require('./boards.json');

const fs = require('fs');

function boardInfo (boardid,callback) {
    
    setTimeout(()=>{
        let result = boards.filter((elem)=>elem['id']==boardid)[0]
        //console.log(result);
        callback(result);

    },2000)

}

module.exports = boardInfo;
