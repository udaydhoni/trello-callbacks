const boards = require("./boards.json");
const lists = require("./lists.json");
const cards = require("./cards.json");


const boardInfo = require('./callback1.js');
const listsOfABoard = require('./callback2.js');
const cardsFromlists = require('./callback3.js');


function usesOtherFunctions3 (boardName) {
    let reqID = boards.filter((elem)=>elem["name"]==boardName)[0]["id"];
    let listID = lists[reqID].reduce((accum,elem)=>{
            accum.push(elem["id"]);
        return accum;
    },[])

    // setTimeout(()=>{
    //     boardInfo(reqID,(x)=>{
    //         console.log(x)
    //     })
    //     listsOfABoard(reqID,(x)=>{
    //         console.log(x)
    //     })
    //     listID.forEach((y)=>{cardsFromlists(y,(x)=>{
    //         console.log(y,x)
    //     })})
    // },0000)

    boardInfo(reqID,(x)=>{
        console.log(x);
        listsOfABoard(reqID,(x)=>{
            console.log(x);
            let listID = [];
            for (let index =0; index<x.length ; index++) {
                    listID.push(x[index]["id"])
            }
            for (let listids of listID) {
                cardsFromlists(listids,(x)=>{
                    console.log(listids,x)
                });
            }
            


            })
        })
    }

module.exports = usesOtherFunctions3;