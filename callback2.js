const lists = require('./lists.json');

function listsOfABoard (boardid,callback) {
    setTimeout(()=>{
    let result = [];
    for (let keys in lists) {
        if (keys == boardid) {
            result = lists[keys]
        }
    }
    callback(result);
    },2000);

}

module.exports = listsOfABoard;
